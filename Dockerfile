#FROM python:3.9-slim
#WORKDIR /app
#COPY task2.py .
#CMD ["python", "task2.py"]

FROM python:3.9-slim
WORKDIR /app
COPY /FastAPI /app
RUN pip install -r ./requirements.txt
ENTRYPOINT ["python", "task2_fastapi.py"]