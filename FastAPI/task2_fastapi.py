from fastapi import FastAPI
from aioredis import Redis
import aioredis
import random
import asyncio

app = FastAPI()

async def get_redis_pool():
    return await aioredis.from_url('redis://redis:6379/0')

@app.on_event("startup")
async def startup_event():
    app.state.redis = await get_redis_pool()
    if not await app.state.redis.exists("array"):
        array = sorted([random.randint(1, 1000) for _ in range(100)])
        await app.state.redis.set("array", str(array))

@app.get("/search/{number}")
async def search_number(number: int):
    array = eval(await app.state.redis.get("array"))
    low = 0
    high = len(array) - 1
    mid = 0

    while low <= high:
        mid = (high + low) // 2

        if array[mid] < number:
            low = mid + 1
        elif array[mid] > number:
            high = mid - 1
        else:
            return {"index": mid}

    return {"index": -1}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8080)
